# Asignación Estratégica de Tiempo de Descanso e Interacción Social

Proyecto Segundo Parcial de la materia Lenguajes de Programación (SOFG1001) Paralelo 1 de la ESPOL.

### Descripción

* Desarrollar una aplicación que determine la condición ambiental laboral actual (adecuada, no-adecuada-th, no-adecuada-ruido) 
* Determinada por los siguientes parámetros: Temperatura, Humedad, Nivel de Ruido
* Los datos serán obtenidos de una base de datos provista para este objetivo
* Se utilizará la librería Incanter para Clojure y SVM-CLJ
* Un clasificador (SVM) permitirá al sistema identificar el momento adecuado en el que los empleados deben ser notificados para tomar un tiempo de descanso

### Integrantes
* Jorge Paladines
* Ricardo Serrano
