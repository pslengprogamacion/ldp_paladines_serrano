(ns ld-cloj-p.core
  (:require [svm.core :as svm :refer :all]
            [incanter.io :as inio]
            [incanter.core :as incore]
            [incanter.datasets :as indata]
            [incanter.stats :as instats]
            [incanter.charts :as incharts]
            [clojure.core.matrix :as mtx])
  (:gen-class)
  (:import (javax.swing SwingUtilities JFrame JLabel JTextField JButton JPanel JOptionPane)
           (java.awt BorderLayout)
           (java.awt FlowLayout)
           (java.awt.event ActionListener)))

  (def data (inio/read-dataset "entrenadorC.csv" :header true))

  (defn mostrarData "Muestra los valores del Archivo entrenador.csv" 
    []
    (incore/view data))

  ;Bloque para la Regresion Lineal Simple de Temperatura - Humedad
  ;Temperatura
  (def temperaturaX (mtx/matrix :persistent-vector
                     (incore/sel data :cols 0)))
  ;Humedad
  (def humedadY (mtx/matrix :persistent-vector
                     (incore/sel data :cols 1)))
  
  (def diagramaDispercion
    (incharts/scatter-plot temperaturaX humedadY
                         :title "Regresión Lineal Simple"
                         :x-label "Temperatura"
                         :y-label "Humedad"))

  (defn mostrarDiagramaDispercion []
    (incore/view diagramaDispercion))

  (def modeloLineal
    (instats/linear-model humedadY temperaturaX))

  (defn mostrarRegLinealTemperaturaHumedad []
    (incore/view (incharts/add-lines diagramaDispercion
                        temperaturaX (:fitted modeloLineal))))


(defn cambiarTemperatura
  "Se le pasa el valor actual de temperatura y lo cambia. El argumento que se le pasa al parámetro temperatura debe ser atómico (atom)"
  [temperatura]
  (def cambio (- (rand-int 7) 3))
  (swap! temperatura + cambio)
  (cond
    (< @temperatura 16) (reset! temperatura 16)
    (> @temperatura 34) (reset! temperatura 34))
  temperatura)


(defn cambiarHumedad
  "Se le pasa el valor actual de humedad y lo cambia. El argumento que se le pasa al parámetro humedad debe ser atómico (atom)"
  [humedad]
  (def cambio (- (rand-int 3) 1))
  (swap! humedad + (/ cambio 100))
  (cond
    (< @humedad 0.30) (reset! humedad 0.30)
    (> @humedad 0.70) (reset! humedad 0.70))
  humedad)
  
(defn cambiarRuido
  "Se le pasa el valor actual de ruido y lo cambia. El argumento que se le pasa al parámetro ruido debe ser atómico (atom)"
  [ruido]
  (def cambio (- (rand-int 11) 5))
  (swap! ruido + cambio)
  (cond
    (< @ruido 40) (reset! ruido 40)
    (> @ruido 89) (reset! ruido 89))
  ruido)

(defn entrenar
  "Entrena al modelo con el archivo entrenador"
  [aEntrenar]
  (def datasetEntrenar (svm/read-dataset aEntrenar))
  (svm/train-model datasetEntrenar))
  
(defn crearMapaValores
  "Se le manda los valores de Temperatura, Humedad y Ruido y los retorna como mapa"
  [temp hum rui]
  (sorted-map 1 temp 2 hum 3 rui))

(defn predecir
  "Función que se encarga de predecir el modelo"
  [model feature]
  (svm/predict model feature))


(defn simularValores
  "Se le manda una lista, y decide cuál de ellos cambiar dependiendo del valor aleatorio. 0->temperatura 1->humedad 2->ruido"
  [temperatura humedad ruido]
  (def indice (rand-int 3))
  (cond
    (= indice 0) (cambiarTemperatura temperatura)
    (= indice 1) (cambiarHumedad humedad)
    (= indice 2) (cambiarRuido ruido)))

(defn obtenerModelo "Obtiene el modelo"
  []
  (def modelo (entrenar "entrenador"))
  modelo)

(defn obtenerMapaValores "Obtiene el mapa de los valores"
  [temperatura humedad ruido]
  (crearMapaValores @temperatura @humedad @ruido))

(defn obtenerClas "Obtiene la clasificacion a partir de la prediccion del modelo"
  [modelo mapaValores]
  (predecir modelo mapaValores))

(defn simulador "Función que simula el programa"
  [modelo clas temperatura humedad ruido mapaValores]
  (simularValores temperatura humedad ruido)
  (def clas (obtenerClas modelo mapaValores))
  (cond
    (= clas 1.0) "Condiciones adecuadas de trabajo. ¡Todo bien!\n"
    (= clas 2.0) "Condiciones críticas de trabajo. La eficacia de sus empleados se verá afectada\n"
    (= clas 3.0) "Temperatura muy alta o muy baja\n"
    (= clas 4.0) "Alta humedad en el ambiente\n"
    (= clas 5.0) "Ruido considerablemente notorio\n"
    (= clas 6.0) "Temperatura inadecuada y alta humedad\n"
    (= clas 7.0) "Temperatura inadecuada y alto ruido\n"
    (= clas 8.0) "        Humedad y ruido muy altos       "))
  

(defn -main
  "I don't do a whole lot ... yet."
  [& args]

  (def t (atom 23))
  (def h (atom 0.4))
  (def r (atom 40))

  (def modelo (obtenerModelo))
  (def mapaValores (crearMapaValores @t @h @r))

  
  
  (defn obtenerMsg []
  (simulador modelo clas t h r mapaValores))


  (let [frame (new JFrame "Ambiente Laboral")     
        lTemperatura (JLabel. "Temperatura: ")
        lSetTemperatura (JLabel. "0")
        lTempUnidad(JLabel. " °C     ")
        lHumedad (JLabel. "Humedad: ")
        lSetHumedad (JLabel. "0")
        lHumUnidad(JLabel. " %     ")
        lRuido (JLabel. "   Ruido: ")
        lSetRuido (JLabel. "0")
        lRuiUnidad(JLabel. "  dB     ")
        lMessage (JLabel. "          msg          ")
        bGrafico (JButton. "               Mostrar Gráfico               ")
        bData (JButton. "       Mostrar Datos Regresión       ")
        panel (JPanel.) panel1 (JPanel.)

        regL (proxy [ActionListener]
        [] ; superclass constructor arguments
        (actionPerformed [e] ; nil below is the parent component
          (mostrarRegLinealTemperaturaHumedad)
          ))
        showD (proxy [ActionListener]
          []
          (actionPerformed [e]
            (mostrarData)))]
    
    (doto panel
      (.add lTemperatura) (.add lSetTemperatura) (.add lTempUnidad)
      (.add lHumedad) (.add lSetHumedad) (.add lHumUnidad)
      (.add lRuido) (.add lSetRuido) (.add lRuiUnidad)
      (.add lMessage)
      (.add bGrafico)
      (.add bData))
    
    (doto frame
      (.setContentPane panel)    
      (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE)
      (.pack)
      (.setSize 500 150)
      (.setResizable false)
      (.setVisible true))
    (.addActionListener bGrafico regL)
    (.addActionListener bData showD)

    

    (while (= 2 2)
      (do
        (. lSetTemperatura (setText (str @(cambiarTemperatura t))))
        (. lSetHumedad (setText (str (format "%.2f" @(cambiarHumedad h)))))
        (. lSetRuido (setText (str @(cambiarRuido r))))
        (def mapaValores (crearMapaValores @t @h @r))
        (. lMessage (setText (obtenerMsg)))
        (Thread/sleep 1000)
      )
    ) 
  )
)
  
  
  
  
  
